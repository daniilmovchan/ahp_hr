from django.http import HttpResponse
from django.shortcuts import render
from django.views import View

from HRD.services.best_employer_service import string_to_float, best_employer_service


class MainView(View):
    template_name = 'main.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        post = request.POST
        dict = string_to_float(post)
        return ResultView.get(ResultView(), request, dict)


class ResultView(View):
    template_name = 'result.html'

    def get(self, request, dict):
        calculation, best_emp, employers = best_employer_service(dict)
        context = {
            'best_emp': best_emp,
            'emp_weights': employers.items(),
            'criteria_weights': calculation['elements']['global_weights'].items(),
            }
        return render(request, self.template_name, context)
