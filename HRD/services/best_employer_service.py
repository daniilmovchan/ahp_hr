from ahpy import ahpy


def best_employer_service(dict):
    comparisons_arr = [
        {('Mich', 'Nick'): dict['exp1'], ('Mich', 'Ser'): dict['exp2'], ('Nick', 'Ser'): dict['exp3']},
        {('Mich', 'Nick'): dict['edu1'], ('Mich', 'Ser'): dict['edu2'], ('Nick', 'Ser'): dict['edu3']},
        {('Mich', 'Nick'): dict['char1'], ('Mich', 'Ser'): dict['char2'], ('Nick', 'Ser'): dict['char3']},
        {('Mich', 'Nick'): dict['age1'], ('Mich', 'Ser'): dict['age2'], ('Nick', 'Ser'): dict['age3']},
    ]
    criteria_arr = {('Experience', 'Education'): dict['crit1'],
                    ('Experience', 'Charisma'): dict['crit2'],
                    ('Experience', 'Age'): dict['crit3'],
                    ('Education', 'Charisma'): dict['crit4'],
                    ('Education', 'Age'): dict['crit5'],
                    ('Charisma', 'Age'): dict['crit6']
                    }

    ahp = AHPEmployer(comparisons_arr, criteria_arr)
    calc = ahp.calc()
    employers = {}
    for item in calc['target_weights']:
        if item == 'Nick':
            employers['Николай'] = calc['target_weights']['Nick']
        if item == 'Mich':
            employers['Михаил'] = calc['target_weights']['Mich']
        if item == 'Ser':
            employers['Сергей'] = calc['target_weights']['Ser']
    return calc, max(employers, key=employers.get), employers


class AHPEmployer:

    def __init__(self, comparisons_arr: [], criteria_arr: dict):
        self.experience_comparisons = comparisons_arr[0]
        self.education_comparisons = comparisons_arr[1]
        self.charisma_comparisons = comparisons_arr[2]
        self.age_comparisons = comparisons_arr[3]
        self.criteria_comparisons = criteria_arr

    def calc(self):
        experience = ahpy.Compare('Experience', self.experience_comparisons, precision=3, random_index='saaty')
        education = ahpy.Compare('Education', self.education_comparisons, precision=3, random_index='saaty')
        charisma = ahpy.Compare('Charisma', self.charisma_comparisons, precision=3, random_index='saaty')
        age = ahpy.Compare('Age', self.age_comparisons, precision=3, random_index='saaty')
        criteria = ahpy.Compare('Criteria', self.criteria_comparisons, precision=3, random_index='saaty')

        criteria.add_children([experience, education, charisma, age])
        report = criteria.report(show=True)
        return report


def string_to_float(dict):
    result = {}
    for item in dict:
        if item != 'csrfmiddlewaretoken':
            result[item] = float(dict[item])
    return result