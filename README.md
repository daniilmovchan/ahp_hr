1. Install python
2. Open CMD and follow next commands:
```
python -m venv venv
.\venv\Scripts\activate
python install -r requirements.txt
cd AHP
python manage.py runserver
```
[main page](./img/1.png)
[result page](./img/2.png)